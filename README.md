

#Instruction: 

Please follow all bellow steps:

composer update

cp .env.example .env


configure database on .env file

create a new database 

php artisan migrate

php artisan key:generate

php artisan jwt:secret

php artisan optimize

Execute: php artisan serve

# API postman link

https://documenter.getpostman.com/view/15931321/2s9Y5YSi2Y
