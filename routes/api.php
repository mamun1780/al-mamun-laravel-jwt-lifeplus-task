<?php

use App\Http\Controllers\API\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


use App\Http\Controllers\API\AuthController;

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout')->middleware('auth:api');;
    Route::post('refresh', 'refresh')->middleware('auth:api');;
});

//Route::any('deposit', [TransactionController::class, 'deposit']);

Route::controller(TransactionController::class)->group(function () {

    Route::post('deposit-store', 'store')->middleware('auth:api');
    Route::post('deposit-list', 'deposit_list')->middleware('auth:api');
    Route::post('withdrawal-store', 'withdrawal_store_demo')->middleware('auth:api');
    Route::post('withdrawal-store', 'withdrawal_store')->middleware('auth:api');
    Route::post('withdrawal-list', 'withdrawal_list')->middleware('auth:api');


});

