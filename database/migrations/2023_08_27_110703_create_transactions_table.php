<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->decimal('amount');

            $table->enum('transaction_type', ['deposit', 'withdrawal']);
            $table->decimal('fee', 8, 2)->default(0.00);
            $table->date('date')->comment('transaction month');
            $table->string('description')->nullable();
            $table->timestamps();


            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }


    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
