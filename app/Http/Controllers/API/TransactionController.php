<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\TransactionService;

class TransactionController extends BaseController
{

    private $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    public function deposit_list()
    {

        $deposit_list = $this->transactionService->transection_list(Auth::id(), 'deposit');
        return $this->sendResponse($deposit_list, 'Deposit List');

    }


    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $request->request->add(['user_id' => Auth::id()]);
            $transection = $this->transactionService->deposit_store($request);
            DB::commit();
            return $this->sendResponse($transection, 'Deposit Successfully added');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->sendError([], 'Deposit Error');
        }


    }


    public function withdrawal_store(Request $request)
    {


        $request->request->add(['user_id' => Auth::id()]);
        $newBalance = $this->transactionService->check_withdrawal_possible(Auth::id(), $request->amount);
        if ($newBalance > 0) {


            $transection = $this->transactionService->withdrawal_store($request);
            return $this->sendResponse($transection, 'Withdrawal successful');
        } else {
            return $this->sendResponse([], 'You do not have enough balance');

        }


    }

    public function withdrawal_list(Request $request){

        $withdrawal_list = $this->transactionService->transection_list(Auth::id(), 'withdrawal');
        return $this->sendResponse($withdrawal_list, 'withdrawal list');
    }


}
